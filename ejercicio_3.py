#Ejercicio 3: Desplaza la llamada de la función
# de nuevo hacia el final,y coloca la definición
# de muestra_estribillo después de la definición
# de repite_estribillo. ¿Qué ocurre cuando haces
# funcionar ese programa?.

#Autora:"Martha Cango"
#Email: "martha.cango@unl.edu.ec"


def repite_estribillo():
    muestra_estribillo()
    muestra_estribillo()

def muestra_estribillo():
    print("Soy un estudiante, que privilegio.")
    print("Estudio todo el dia y duermo en la noche.")

repite_estribillo()

#Presenta por pantalla:
# Soy un estudiante, que privilegio.
# Estudio todo el el dia y duermo en la noche.
# Soy un estudiante, que privilegio.
# Estudio todo el dia y duermo en la noche.
#Es decir no cambia nada el mensaje, se presenta igual
# que poner al inicio o despues a def muestra_estribillo()