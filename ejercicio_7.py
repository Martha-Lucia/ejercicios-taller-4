#Ejercicio 7: Reescribe el programa de calificaciones del
#capítulo anterior usando una función llamada calcula_calificacion,
#que reciba una puntuación como parámetro y devuelva una calificación
# como cadena.

#__autora__  = "Martha Cango"
#__email__   = "martha.cango@unl.edu.ec"

while 1:
    try:
        puntuacion = float(input("Introduza puntuación:"))
        # validación del rango  de la puntuación


        def calculo_calificacion(puntuacion):
            if puntuacion >= 0 and puntuacion <= 1.0:
                if puntuacion > 0.9:
                    print("Calificación Sobresaliente")
                elif puntuacion >0.8:
                    print("Calificación Notable")
                elif puntuacion >0.7:
                    print("Calificación Bien")
                elif puntuacion >0.6:
                    print("Calificación Suficiente")
                elif puntuacion <=0.6:
                    print("Calificación Insuficiente")
            else:
                print("Puntuación incorrecta")
                return puntuacion


        calculo_calificacion(puntuacion)

    except ValueError:
        print("Puntuación incorrecta")

















