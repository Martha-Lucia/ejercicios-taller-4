#Ejercicio 2: Desplaza la última línea del programa
# anterior hacia arriba,de modo que la llamada a la
# función aparezca antes que las definiciones. Ejecuta
# el programa y observa qué mensaje de error obtienes.

#Autora:"Martha Cango"
#Email: "martha.cango@unl.edu.ec"

repite_estribillo()

def muestra_estribillo():
    print("Soy un estudiante, que privilegio.")
    print("Estudio todo el dia y duermo en la noche.")


def repite_estribillo():
    muestra_estribillo()
    muestra_estribillo()


#El mensaje de error que presenta el programa es NameError: porque
#el nombre 'repite_estribillo' no está definido.